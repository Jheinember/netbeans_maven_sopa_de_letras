package Negocio;



public class SopaDeLetras
{    
    private char sopas[][];
   

    
    public SopaDeLetras(){
        
    }
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error  no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     { 
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    {    
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                int filas = this.sopas[i].length;
                int columnas = this.sopas.length;
                if (filas == columnas) {
                    return true;
                }
            }
        }
        return false;
    }
    
    
     public boolean dispersa()            
    {
        int tamaño= this.sopas[0].length;
        for (int i = 1; i < this.sopas.length; i++) {
            if(this.sopas[i].length !=tamaño){
                return true;
            }
            
        }
        
        return false;
    }
    
    public boolean esRectangular()
    {
        return sopas.length<sopas[0].length || sopas.length<sopas[0].length;
      
    }
    
    
    
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    
    
      
    public int getContar(String palabra)
    {
        int contPalabra = 0;
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {

                char aux = palabra.charAt(0);
                if (aux == sopas[i][j]) {
                        int auxf = i;
                        int auxc = j+1;
                        boolean x = false;
                        String auxPal=sopas[i][j]+"";
                    for (int k = 1; k < palabra.length() && auxc < sopas[i].length&&!x; k++, auxc++) {
                        if(palabra.charAt(k)!=sopas[i][auxc]){
                            x = true;
                        }else{
                              auxPal +=sopas[i][auxc];   
                        }
                        
                    }
                    if(auxPal.equals(auxPal)){
                        contPalabra++;
                    }
                    
                }                 
            }            
        }     
        return 0;
    }
    
    
    
    public int encontrarPalabras(String palabra)
    {
        int contPalabra = 0;
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                char aux = palabra.charAt(0);
                if (aux == sopas[i][j]) {
                       if(buscarHorizontal(palabra,j+1,i)==1){
                       contPalabra++;
                       } else if(buscarVertical(palabra,i+1,j)==1){
                       contPalabra++;
                       } else if(buscarDiagonal(j+1,i+1,palabra)==1){
                       contPalabra++;
                       }
                }                 
            }            
        }     
        return contPalabra;
    }
    
    public int buscarHorizontal(String palabra,int auxc,int i){
        int auxIzquierda=auxc-2;
        String auxPal=palabra.charAt(0)+"";
        boolean x=false;
        for (int k = 1; k < palabra.length() && auxc < sopas[i].length  && !x; k++, auxc++) {
            if(palabra.charAt(k)!=sopas[i][auxc]){
                x = true;
            }else{
                  auxPal +=sopas[i][auxc];   
            }            
    }
        
        
        if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+i+ " - "+(auxc-palabra.length())+" horizontal de izq a derecha" );
            return 1;
        }else
             auxPal=palabra.charAt(0)+"";
            x = false;
            auxc = auxIzquierda;
            for (int k = 1; k < palabra.length() && auxc >= 0 && !x; k++, auxc--) {
                if (palabra.charAt(k) != sopas[i][auxc]) {
                    x = true;
                } else {
                    auxPal += sopas[i][auxc];
                }

            }
            if(auxPal.equals(palabra)){ 
                System.out.println("palabra encontrada en: "+i+" - "+(auxIzquierda+1)+" horizontal invertida de der a izq" );
                return 1;
            }
               return 0;

    }
    
    public int buscarVertical(String palabra,int auxf,int j){
        int auxSubida=auxf-2;
        String auxPal=palabra.charAt(0)+"";
        boolean x=false;
        for (int k = 1; k < palabra.length() && auxf < sopas.length  && !x; k++, auxf++) {
                        if(palabra.charAt(k)!=sopas[auxf][j]){
                            x = true;
                        }else{
                              auxPal +=sopas[auxf][j];   
                        }
                        
                    }
        
        
        if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+(auxSubida+1)+ " - "+j+" Vertical de arriba hacia abajo" );
            return 1;
        }else
             auxPal=palabra.charAt(0)+"";
            x = false;
            auxf = auxSubida;
            for (int k = 1; k < palabra.length() && auxf >= 0 && !x; k++, auxf--) {
                if (palabra.charAt(k) != sopas[auxf][j]) {
                    x = true;
                } else {
                    auxPal += sopas[auxf][j];
                }

            }
            if(auxPal.equals(palabra)){ 
                System.out.println("palabra encontrada en: "+(auxSubida+1)+ " - "+j+" Vertical Invertida de abajo hacia arriba" );
                return 1;
            }
               return 0;

    }
    
    public int buscarDiagonal(int auxc,int auxf,String palabra){
        
        int auxCol=auxc-2;
        int auxFil=auxf-2;

        String auxPal=palabra.charAt(0)+"";
        boolean x=false;
        
        // Diagonal abajo derecha
        for (int k = 1; k <palabra.length()&& auxf<sopas.length&&auxc< sopas[auxf].length &&!x
                ; k++,auxc++,auxf++){
            
                        if(palabra.charAt(k)!=sopas[auxf][auxc]){
                            x = true;
                        }else{
                              auxPal +=sopas[auxf][auxc];   
                        }
        }
            //System.out.println(auxPal);
        if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+(auxFil+1)+ " - "+(auxCol+1)+" Vertical hacia Abajo-derecha" );
            return 1;
        }else{
            auxPal=palabra.charAt(0)+"";
        }
        
        // Diagonal abajo izquierda  
        auxf=auxFil+2;
        auxc=auxCol;
        x=false;
       for (int k = 1; k <palabra.length() && auxf<sopas.length && auxc>=0 && !x 
            ; k++,auxc--,auxf++){
            
                        if(palabra.charAt(k)!=sopas[auxf][auxc]){
                            x = true;
                        }else{
                              auxPal +=sopas[auxf][auxc];   
                        }
        }
            //System.out.println(auxPal);
       if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+(auxFil+1)+ " - "+(auxCol+1)+" Vertical hacia Abajo-izquierda" );
            return 1;
        }else{
            auxPal=palabra.charAt(0)+"";
        }
       
       // Diagonal arriba izquierda  
        auxf=auxFil;
        auxc=auxCol;
        x=false;
       for (int k = 1; k <palabra.length() && auxf>=0 && auxc>=0 && !x 
            ; k++,auxc--,auxf--){
            
                        if(palabra.charAt(k)!=sopas[auxf][auxc]){
                            x = true;
                        }else{
                              auxPal +=sopas[auxf][auxc];   
                        }
        }
       //System.out.println(auxPal);
       if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+(auxFil+1)+ " - "+(auxCol+1)+" Vertical hacia arrida-izquierda" );
            return 1;
        }else{
            auxPal=palabra.charAt(0)+"";
        }
       
        // Diagonal arriba derecha  
        auxf=auxFil;
        auxc=auxCol+2;
        x=false;
       for (int k = 1; k <palabra.length() && auxf>=0 && auxc<sopas[auxf].length && !x 
            ; k++,auxc++,auxf--){
            
                        if(palabra.charAt(k)!=sopas[auxf][auxc]){
                            x = true;
                        }else{
                              auxPal +=sopas[auxf][auxc];   
                        }
        }
        //System.out.println(auxPal);
       if(auxPal.equals(palabra)){
            System.out.println("palabra encontrada en: "+(auxFil+1)+ " - "+(auxCol+1)+" Vertical hacia arriba-derecha" );
            return 1;
        }
       
       return 0;
       
       
       
    }
    /*
        debe ser cuadrada sopas
          */
    public char []getDiagonalPrincipal() throws Exception
    {
        if(!esCuadrada()){       
            throw new Exception();    
        }else{
            char cadenaP []= new char [sopas.length];
            
            for (int i = 0; i < sopas.length; i++) {
                cadenaP[i]=sopas[i][i];
                
            }
            return cadenaP;
        }
       
    }
    
        

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!

    public void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}